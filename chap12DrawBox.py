from tkinter import *
import random
import time


def random_rectangle(width, height, fill_color):
    x1 = random.randrange(width)
    y1 = random.randrange(height)
    x2 = x1 + random.randrange(width)
    y2 = y1 + random.randrange(height)
    canvas.create_rectangle(x1, y1, x2, y2, fill=fill_color)


def movetriangle(event):
    canvas.move(1, 5, 0)

def movetriangle2(event):
    if event.keysym == "Up":
        canvas.move(1, 0, -3)
    elif event.keysym == "Down":
        canvas.move(1, 0, 3)
    elif event.keysym == "Left":
        canvas.move(1, -3, 0)
    else:
        canvas.move(1, 3, 0)

#c = colorchooser.askcolor()
testing = Tk()
testing.title("Oh Yeah!!!")
canvas = Canvas(testing, width=400, height=400)
canvas.pack()
#random_rectangle(400, 400, c[1])
#for x in range(0, 100):
#    random_rectangle(400, 400, c[x])


#canvas.create_arc(10, 10, 200, 100, extent=180, style=ARC)
#canvas.create_arc(10, 10, 200, 80, extent=45, style=ARC)
#canvas.create_arc(10, 80, 200, 160, extent=90, style=ARC)
#canvas.create_arc(10, 160, 200, 240, extent=135, style=ARC)
#canvas.create_arc(10, 240, 200, 320, extent=180, style=ARC)
#canvas.create_arc(10, 320, 200, 400, extent=359, style=ARC)

#canvas.create_text(150, 100, text="There once was a man from Toulouse,")
#canvas.create_text(130, 120, text="Who rode on a moose.", fill='red')




#Animation portion

myTri = canvas.create_polygon(10, 10, 10, 60, 50, 35)

for x in range(0, 60):
    canvas.move(1, 5, 5)
    testing.update()
    time.sleep(0.05)

for x in range(0, 60):
    canvas.move(1, -5, -5)
    testing.update()
    time.sleep(0.05)

canvas.bind_all('<KeyPress-Return>', movetriangle)
canvas.bind_all('<KeyPress-Up>', movetriangle2)
canvas.bind_all('<KeyPress-Down>', movetriangle2)
canvas.bind_all('<KeyPress-Left>', movetriangle2)
canvas.bind_all('<KeyPress-Right>', movetriangle2)

canvas.itemconfig(myTri, fill="blue")
canvas.itemconfig(myTri, outline="red")
testing.mainloop()