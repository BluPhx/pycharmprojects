def string_match(a, b):
    match = 0
    counter = len(b)
    if len(a) < counter:
        counter = len(a)

    for x in range(counter - 1):
        if a[x:x+2] == b[x:x+2]:
            match += 1
    return match
